const TermsModel = require("../schema/terms.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");

class TermsService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
}


module.exports = new TermsService(TermsModel, 'terms');
