const FeesdataModel = require("../schema/feesdata.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");

class FeesdataService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
    getAllRequestsByCriteria(criteria) {

        const query = {};

        if(criteria.payment_mode) {
            query.payment_mode = criteria.payment_mode
        }

       
        return this.getAllByCriteria(query);
    }
    
}

module.exports = new FeesdataService(FeesdataModel, 'feesdata');
