const { default: mongoose } = require("mongoose");
const FeesdetailsModel = require("../schema/feesdetails.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");

class FeesdetailsService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
   
}

module.exports = new FeesdetailsService(FeesdetailsModel, 'feesdetails');
