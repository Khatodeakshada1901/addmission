const EnquiryModel = require("../schema/enquiry.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");

class EnquiryService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
    getAllRequestsByCriteria(criteria) {

        const query = {};

        if(criteria.user_name) {
            query.user_name = criteria.user_name
        }

       
        return this.getAllByCriteria(query);
    }
}

module.exports = new EnquiryService(EnquiryModel, 'enquiry');
