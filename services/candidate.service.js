const { query } = require("express");
const CandidateModel = require("../schema/candidate.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");

class CandidateService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }

    // getAllRequestByCriteria(criteria) {
    //     let query = {};

    //     if (criteria.user_name) {
    //         query.user_name = criteria.user_name;
    //     }

    //     return this.getAllByCriteria(query);
    // }

    getAllRequestsByCriteria(criteria) {
        const query = {};
    
        if (criteria.user_name) {
            query['personalinfo.user_name'] = new RegExp(criteria.user_name, 'i');
          
        }
        if (criteria.enquiryId) {
           query.enquiryId=criteria.enquiryId
          
        }
        if (criteria.mobile) {
            query['personalinfo.mobile'] = criteria.mobile;
          
        }
          return this.getAllByCriteria(query);
    }
}

// findByName: async (user_name) => {
//     const data = await Course.find({
//         user_name: { $regex : '.*'+  user_name + '.*', $options:'i'} ,
//     });
//     return data;
//   }

module.exports = new CandidateService(CandidateModel, "candidate");
