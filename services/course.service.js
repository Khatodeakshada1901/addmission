const CourseModel = require("../schema/course.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");

class CourseService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
}


module.exports = new CourseService(CourseModel, 'course');
