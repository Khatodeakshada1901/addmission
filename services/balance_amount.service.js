const Balance_amountModel = require("../schema/balance_amount.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");
const courseModel = require("../schema/course.schema");
class Balance_amountService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
    getRemainingFees(course_id, paid_amount) {
        return new Promise(async (resolve, reject) => {
            try {
                const course = await courseModel.findById(course_id);
                if (!course) {
                    reject({
                        status: 404,
                        success: false,
                        message: "Course not found",
                    });
                }

                let remaining_fees = course.total_fees - paid_amount;

                resolve({
                    status: 200,
                    success: true,
                    data: { remaining_fees },
                    message: "Fetched data successfully",
                });
            } catch (error) {
                console.log("Error while fetching remaining fees", error);
                reject({
                    status: 500,
                    success: false,
                    message: "Server Error",
                });
            }
        });
    }
}

module.exports = new Balance_amountService(
    Balance_amountModel,
    "balance_amount"
);
