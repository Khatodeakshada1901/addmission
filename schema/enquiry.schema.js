const mongoose = require("mongoose");

const EnquirySchema = new mongoose.Schema(
    {
       
        user_name: {
            type: String,
            required: true,
        },
        mobile: {
            type:Number,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        course: {
            type: String,
            required: true,
        }

    },
    { timestamps: true }
);

const EnquiryModel = mongoose.model("enquiry", EnquirySchema);
module.exports = EnquiryModel;
