const mongoose = require("mongoose");

const FeesdetailsSchema = new mongoose.Schema(
    {
        candidate_id: {
            type: "string",
            required: false,
        },
    },
    { strict: false, timestamps: true }
);

const FeesdetailsModel = mongoose.model("feesdetails", FeesdetailsSchema);
module.exports = FeesdetailsModel;
