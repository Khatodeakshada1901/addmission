const mongoose = require("mongoose");

const TermsSchema = new mongoose.Schema(
    {
        Year: {
            type: Number,
            required: true,
        }
    },
    { timestamps: true }
);


const TermsModel = mongoose.model("terms", TermsSchema);
module.exports = TermsModel;
