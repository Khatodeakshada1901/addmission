const mongoose = require("mongoose");
const { schema } = require("./feesdata.schema");

const CandidateSchema = new mongoose.Schema(
    {
    
        // personalinfo: {
        //     user_name: {
        //         type: String,
        //         required: true,
        //     },
        //     email: {
        //         type: String,
        //         required: true,
        //     },
        //     p_city: {
        //         type: String,
        //         required: true,
        //     },
        //     p_house: {
        //         type: String,
        //         required: true,
        //     },
        //     p_town: {
        //         type: String,
        //         required: true,
        //     },
        //     p_state: {
        //         type: String,
        //         required: true,
        //     },
        //     p_zipcode: {
        //         type: Number,
        //         required: true,
        //     },
        //     mobile: {
        //         type: Number,
        //         required: true,
        //     },
        //     gender: {
        //         type: String,
        //         required: true,
        //     },
        // },
        // address: {
        //     c_house: {
        //         type: String,
        //         required: true,
        //     },
        //     c_town: {
        //         type: String,
        //         required: true,
        //     },
        //     c_city: {
        //         type: String,
        //         required: true,
        //     },
        //     c_state: {
        //         type: String,
        //         required: true,
        //     },
        //     c_zipcode: {
        //         type: Number,
        //         required: true,
        //     },
        //     father_mobile: {
        //         type: Number,
        //         required: true,
        //     },
        //     mother_mobile: {
        //         type: Number,
        //         required: true,
        //     },
        //     emergency_contact: {
        //         type: Number,
        //         required: true,
        //     },
        // },
        // academicinfo: {
        //     school_name: {
        //         type: String,
        //         required: true,
        //     },

        //     s_percentage: {
        //         type: Number,
        //         required: true,
        //     },
        //     s_year_of_passing: {
        //         type: Number,
        //         required: true,
        //     },
        //     s_board: {
        //         type: String,
        //         required: true,
        //     },
        //     medium: {
        //         type: String,
        //         required: true,
        //     },
        //     hs_college_name: {
        //         type: String,
        //         required: true,
        //     },

        //     hs_percentage: {
        //         type: Number,
        //         required: true,
        //     },
        //     hs_year_of_passing: {
        //         type: Number,
        //         required: true,
        //     },
        //     hs_board: {
        //         type: String,
        //         required: true,
        //     },
        //     hs_stream: {
        //         type: String,
        //         required: true,
        //     },
        //     hs_college_name: {
        //         type: String,
        //         required: true,
        //     },

        //     hs_percentage: {
        //         type: Number,
        //         required: true,
        //     },
        //     hs_year_of_passing: {
        //         type: Number,
        //         required: true,
        //     },
        //     hs_board: {
        //         type: String,
        //         required: true,
        //     },
        //     hs_stream: {
        //         type: String,
        //         required: true,
        //     },
        //     ug_college_name: {
        //         type: String,
        //         required: true,
        //     },

        //     ug_percentage: {
        //         type: Number,
        //         required: true,
        //     },
        //     ug_year_of_passing: {
        //         type: Number,
        //         required: true,
        //     },
        //     ug_university: {
        //         type: String,
        //         required: true,
        //     },
        //     ug_stream: {
        //         type: String,
        //         required: true,
        //     },
        //     ug_specialization: {
        //         type: String,
        //         required: true,
        //     },
        //     pg_college_name: {
        //         type: String,
        //         required: true,
        //     },

        //     pg_percentage: {
        //         type: Number,
        //         required: true,
        //     },
        //     pg_year_of_passing: {
        //         type: Number,
        //         required: true,
        //     },
        //     pg_university: {
        //         type: String,
        //         required: true,
        //     },
        //     pg_stream: {
        //         type: String,
        //         required: true,
        //     },
        //     pg_specialization: {
        //         type: String,
        //         required: true,
        //     },
        // },
        // familyinfo: {
        //     father_full_name: {
        //         type: String,
        //         required: true,
        //     },
        //     f_occupation: {
        //         type: String,
        //         required: true,
        //     },
        //     f_yearly_income: {
        //         type: Number,
        //         required: true,
        //     },
        //     f_total_land: {
        //         type: String,
        //         required: true,
        //     },
        //     mother_full_name: {
        //         type: String,
        //         required: true,
        //     },
        //     occupation: {
        //         type: String,
        //         required: true,
        //     },
        //     yearly_income: {
        //         type: Number,
        //         required: true,
        //     },
        //     total_land: {
        //         type: String,
        //         required: true,
        //     },
        //     no_of_brothers: {
        //         type: Number,
        //         required: true,
        //     },
        //     no_of_sisters: {
        //         type: Number,
        //         required: true,
        //     },
        //     approximate_income: {
        //         type: Number,
        //         required: true,
        //     },
        //     working_it: {
        //         type: String,
        //         required: true,
        //     },
        // },
        // document: {
        //     pan_card: {
        //         type: String,
        //         required: true,
        //     },
        //     adharcard_no: {
        //         type: Number,
        //         required: true,
        //     },
        //     passport_no: {
        //         type: String,
        //         required: true,
        //     },
        // },
        // hostel: {
        //     hostel_interested: {
        //         type: String,
        //         required: true,
        //     },
        // },
    },
    { strict:false, timestamps: true }
);

const CandidateModel = mongoose.model("candidate", CandidateSchema);
module.exports = CandidateModel;
