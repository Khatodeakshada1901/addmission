const mongoose = require("mongoose");

const CourseSchema = new mongoose.Schema(
    {
        courseName: {
            type: String,
            required: true,
        }
    },
    {strict:false, timestamps: true }
);


const CourseModel = mongoose.model("course", CourseSchema);
module.exports = CourseModel;
