const mongoose = require("mongoose");

const Balance_amountSchema = new mongoose.Schema(
    {},
    { strict: false, timestamps: true }
);

const Balance_amountModel = mongoose.model(
    "balance_amount",
    Balance_amountSchema
);
module.exports = Balance_amountModel;
