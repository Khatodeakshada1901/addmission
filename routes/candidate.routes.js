const express = require("express");
const router = express.Router();
const { checkSchema } = require("express-validator");
const service = require("../services/candidate.service");
const requestResponsehelper = require("@baapcompany/core-api/helpers/requestResponse.helper");
const ValidationHelper = require("@baapcompany/core-api/helpers/validation.helper");

router.post(
    "/",
    checkSchema(require("../dto/candidate.dto")),
    async (req, res, next) => {
        if (ValidationHelper.requestValidationErrors(req, res)) {
            return;
        }

        // Generate the auto ID
        const candidate_id = +Date.now();

        // Set the generated ID in the request body
        req.body.candidate_id = candidate_id;

        const serviceResponse = await service.create(req.body);
        requestResponsehelper.sendResponse(res, serviceResponse);
    }
);
router.delete("/:id", async (req, res) => {
    const serviceResponse = await service.deleteById(req.params.id);

    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.put("/:id", async (req, res) => {
    const serviceResponse = await service.updateById(req.params.id, req.body);

    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.get("/:id", async (req, res) => {
    const serviceResponse = await service.getById(req.params.id);

    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.get('/all/candidate', async (req, res) => {
    const serviceResponse = await service.getAllRequestsByCriteria({
        user_name: req.query.user_name,
        enquiryId:req.query.enquiryId,
        mobile:req.query.mobile
    });

    requestResponsehelper.sendResponse(res, serviceResponse);
});


module.exports = router;
