const express = require("express");
const router = express.Router();
const { checkSchema } = require("express-validator");
const service = require("../services/balance_amount.service");
const requestResponsehelper = require("@baapcompany/core-api/helpers/requestResponse.helper");
const ValidationHelper = require("@baapcompany/core-api/helpers/validation.helper");

router.post(
    "/",
    checkSchema(require("../dto/balance_amount.dto")),
    async (req, res, next) => {
        if (ValidationHelper.requestValidationErrors(req, res)) {
            return;
        }
        const serviceResponse = await service.create(req.body);
        requestResponsehelper.sendResponse(res, serviceResponse);
    }
);
// router.get("/courses/:id/remaining_fees", async (req, res) => {
//     try {
//         const course_id = req.params.id;
//         const paid_amount = req.query.paid_amount;
//         const result = await service.getRemainingFees(course_id, paid_amount);
//         res.status(result.status).json(result);
//     } catch (error) {
//         res.status(error.status || 500).json(error);
//     }
// });
router.get("/courses/:id", async (req, res) => {
    const course_id = req.params.id;
    const paid_amount = req.query.paid_amount;
    const serviceResponse = await service.getRemainingFees(
        course_id,
        paid_amount
    );

    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.delete("/:id", async (req, res) => {
    const serviceResponse = await service.deleteById(req.params.id);

    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.put("/:id", async (req, res) => {
    const serviceResponse = await service.updateById(req.params.id, req.body);

    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.get("/:id", async (req, res) => {
    const serviceResponse = await service.getById(req.params.id);

    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.get("/all/balance_amount", async (req, res) => {
    const serviceResponse = await service.getAllByCriteria({});

    requestResponsehelper.sendResponse(res, serviceResponse);
});

module.exports = router;
